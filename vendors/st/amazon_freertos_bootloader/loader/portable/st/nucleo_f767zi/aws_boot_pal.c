/**
  ******************************************************************************
  * File Name          : aws_boot_pal.c
  * Description        : This file provides code that defines standard portable
  *                      functions for the bootloader of the NUCLEO-H743ZI.
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2018 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */


/**
 * @file aws_boot_pal.c
 * @brief Bootloader platform code.
 */

/* ST framework includes. */
#include "stm32f7xx_hal.h"

/* Bootloader includes. */
#include "aws_boot_types.h"
#include "aws_boot_partition.h"
#include "aws_boot_loader.h"
#include "aws_boot_log.h"
#include "aws_boot_config.h"

IWDG_HandleTypeDef hiwdg1;
/*-----------------------------------------------------------*/

void BOOT_PAL_LaunchApplication( const void * const pvLaunchAddress )
{
    void (* pfApplicationEntry)( void ) = ( void ( * )( void ) ) (* (uint32_t volatile *) ((uint32_t) pvLaunchAddress + 4));
    
    __set_MSP(* (uint32_t *) pvLaunchAddress);

    /* Disable any interrupts. */
    __disable_irq();
    
    /* Launch...*/
    ( *pfApplicationEntry )();
}

/*-----------------------------------------------------------*/

void BOOT_PAL_LaunchApplicationDesc( const BOOTImageDescriptor_t * const pvLaunchDescriptor )
{
    DEFINE_BOOT_METHOD_NAME( "BOOT_PAL_LaunchApplicationDesc" );
    
    void * pvExecAddress = pvLaunchDescriptor->pvExecAddress;
    
    BOOT_LOG_L1( "\n[%s] Booting image. \r\n", BOOT_METHOD_NAME );
    
    BOOT_PAL_LaunchApplication( pvExecAddress );
}

/*-----------------------------------------------------------*/

BaseType_t BOOT_PAL_WatchdogInit( void )
{
    DEFINE_BOOT_METHOD_NAME( "BOOT_PAL_WatchdogInit" );
    
    /*
     * Bootloader watchdog initializations here.
     */

    /*
     * No initialization required for watchdog.
     */
    BOOT_LOG_L1( "\n[%s] Watchdog initialization complete. \r\n", BOOT_METHOD_NAME );

    return pdTRUE;
}

/*-----------------------------------------------------------*/

BaseType_t BOOT_PAL_WatchdogEnable( void )
{
    DEFINE_BOOT_METHOD_NAME( "BOOT_PAL_WatchdogEnable" );
    
    BaseType_t xReturn = pdTRUE;

    hiwdg1.Instance = IWDG;
    hiwdg1.Init.Prescaler = IWDG_PRESCALER_256;
    hiwdg1.Init.Reload = (32000 * 60000) / (256 * 1000); /* 60 s */
    hiwdg1.Init.Window = IWDG_WINDOW_DISABLE;
    
    if (HAL_IWDG_Init(&hiwdg1) != HAL_OK)
    {
      BOOT_LOG_L1( "\n[%s] Error enabling the watchdog. \r\n", BOOT_METHOD_NAME );
      xReturn = pdFALSE;
    }
    
    return xReturn;
}

/*-----------------------------------------------------------*/

BaseType_t BOOT_PAL_WatchdogDisable( void )
{
    DEFINE_BOOT_METHOD_NAME( "BOOT_PAL_WatchdogEnable" );
    
    /*
     * The watchdog cannot be disabled other than by a reset.
     */
    BOOT_LOG_L1( "\n[%s] The watchdog cannot be disabled other than by a system reset. \r\n", BOOT_METHOD_NAME );

    return pdFALSE;
}

/*-----------------------------------------------------------*/

void BOOT_PAL_NotifyBootError( void )
{
  
}

/*-----------------------------------------------------------*/

uint8_t BOOT_PAL_GetImageFlags( const BOOTImageDescriptor_t * const pxAppDescriptor )
{
    uint8_t ucImageFlags = 0;

    if (pxAppDescriptor->xImageHeader.ucImageValidFlag == 0)
    {
        ucImageFlags = eBootImageFlagValid;
    }
    else if (pxAppDescriptor->xImageHeader.ucImageCommitPendingFlag == 0)
    {
        ucImageFlags = eBootImageFlagCommitPending;
    }
    else if (pxAppDescriptor->xImageHeader.ucImageNewFlag == 0)
    {
        ucImageFlags = eBootImageFlagNew;
    }

    return ucImageFlags;
}

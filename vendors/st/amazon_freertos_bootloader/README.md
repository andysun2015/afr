# Amazon FreeRTOS Reference Bootloader for the NUCLEO-H743ZI ##
The reference bootloader demonstrates features for supporting OTA and crypto signature verification on boot.

**Note that currently the st reference bootloader works on NUCLEO-h&43ZI only.**

## Building the bootloader

* Install python 3 or higher  
  [Python Download](https://www.python.org/downloads/)  
  Check if you can run command: `python --version`, it should output 3.x.x
    
* Install pip if not already available on system  
   follow instructions at  https://pip.pypa.io/en/stable/installing/  

* Install pyopenssl by executing  
`pip install pyopenssl`

* Put the ECDSA code signer certificate in pem format in the path - 
    `\demos\common\ota\st_bootloader\utility\codesigner_cert_utility`  
    rename the cert to `aws_ota_codesigner_certificate.pem`  
    
* The codesigner_cert_utility.py runs as the pre build step for the bootloader 
project and extracts the public key in `aws_boot_codesigner_public_key.h` which
is then linked in the bootloader. 

## Building the OTA application image

*  Update the application version number in `\demos\common\include\aws_application_version.h`  
    
*  Build the aws_bootloader project. Flash the generated aws_bootloader.hex to 0x08000000.

*  Build the aws_demos project. Generate the required Factory and OTA binaries using factory_image_generator.py. 

```
python factory_image_generator.py -b aws_demos.bin -p NUCLEO-H743ZI -k test_private_key.pem   
```
    -aws_demos.bin is the application binary (this is not OTA binary)  
    -NUCLEO-H743ZI is the platform name  
    -test_private_key.pem is the private key 

*  For debug builds, running the factory image generation script is not required. For production builds, running the factory image generation script is necessary. In production, the factory image should be signed with the bootloader verifying its crypto signature.

## Flashing the factory image

* Using the ST-LINK Utility, connect to the device.

* Erase the device.

* Load the generated bootloader hex file i.e. aws_bootloader.hex.

* Click program and wait for programming to complete.

* Load the generated factory image bin file to address 0x0804000 i.e. aws_demos.initial.bin.

* Click program and wait for programming to complete.

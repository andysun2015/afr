/**
  ******************************************************************************
  * File Name          : aws_boot_flash.h
  * Description        : This file provides code that interacts with the FLASH
  *                      memory on the STM32H743ZI.
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2018 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/**
 * @file aws_boot_flash_info.h
 * @brief Boot flash info definitions.
 */

#ifndef _AWS_BOOT_FLASH_INFO_H_
#define _AWS_BOOT_FLASH_INFO_H_

/*ST includes.*/
#include "stm32f7xx_hal.h"

/**
 * @brief Minimum write size of Flash.
 */
//#define FLASHWORD_SIZE      ( 4U )

/**
 * @brief Flash device ID.
 */
#define FLASH_DEVICE_INTERNAL             ( 0U )

// product define
#define FLASH_SIZE         0x200000UL          /* 2 MB   */
#define FLASH_BANK_SIZE    (FLASH_SIZE >> 1)   /* 1 MB   */

/**
 * @brief Flash device base.
 */
#define FLASH_BANK1_BASE          			(0x08000000UL) /*!< Base address of : (up to 1 MB) Flash Bank1 accessible over AXI */
#define FLASH_DEVICE_BASE                 	( FLASH_BANK1_BASE )

/**
 * @brief Flash partitions for OTA images.
 */
#define FLASH_PARTITION_IMAGE_0           ( 0U )
#define FLASH_PARTITION_IMAGE_1           ( 1U )
#define FLASH_PARTITION_IMAGE_BACKUP      ( 2U )

/**
 * @brief Flash partition sector size.
 */
#define FLASH_SECTOR_SIZE  0x00020000UL        /* 128 KB */

#define FLASH_PARTITION_IMAGE_PAGES_SIZE			( FLASH_SECTOR_SIZE )
#define FLASH_PARTITION_IMAGE_PAGES_PER_SECTOR		( 1U )
#define FLASH_PARTITION_IMAGE_SECTOR_SIZE     		( FLASH_PARTITION_IMAGE_PAGES_PER_SECTOR * FLASH_PARTITION_IMAGE_PAGES_SIZE )

/**
 * @brief Flash partition offsets.
 */
// 128K * 2 = 256K bootloader
#define FLASH_PARTITION_OFFSET_IMAGE_0        ( 2U * FLASH_PARTITION_IMAGE_SECTOR_SIZE )
// ota image start with 128K * 2 address - sector 18
#define FLASH_PARTITION_OFFSET_IMAGE_1        ( FLASH_BANK_SIZE + 2U * FLASH_PARTITION_IMAGE_SECTOR_SIZE )
// backup image start with 128K * 3 address - sector 19
#define FLASH_PARTITION_OFFSET_IMAGE_BACKUP   ( FLASH_BANK_SIZE + 3U * FLASH_PARTITION_IMAGE_SECTOR_SIZE )

/**
 * @brief Maximum flash partitions for OTA images.
 */
#define FLASH_PARTITIONS_OTA_MAX          ( 3U )

/**
 * @brief Flash partition size.
 */
// 128K * 6 = 768K
#define FLASH_IMAGE_SIZE_MAX              ( 6U * FLASH_PARTITION_IMAGE_SECTOR_SIZE )

/**
 * @brief Flash partition definitions.
 */
#define MAIN_IMAGE_BASE				    			( FLASH_DEVICE_BASE + FLASH_PARTITION_OFFSET_IMAGE_0 )
#define OTA_IMAGE_BASE				    			( FLASH_DEVICE_BASE + FLASH_PARTITION_OFFSET_IMAGE_1 )
#define BACKUP_IMAGE_BASE							( FLASH_DEVICE_BASE + FLASH_PARTITION_OFFSET_IMAGE_BACKUP )

/**
 * @brief Flash sector definitions.
 */
#define IMAGE_SECTOR_0								( 0U )
#define IMAGE_SECTOR_1								( 1U )
#define IMAGE_SECTOR_2								( 2U )
#define IMAGE_SECTOR_3								( 3U )
#define IMAGE_SECTOR_4								( 4U )
#define IMAGE_SECTOR_5								( 5U )

/**
 * @brief Flash sector manipulation macros.
 */
#define IMAGE_SECTOR_OFFSET(SECTOR)         		( ( SECTOR ) * FLASH_PARTITION_IMAGE_SECTOR_SIZE )

#define MAIN_IMAGE_SECTOR_ADDRESS( IMAGE_SECTOR )	( MAIN_IMAGE_BASE + IMAGE_SECTOR_OFFSET( IMAGE_SECTOR ) )
#define OTA_IMAGE_SECTOR_ADDRESS( IMAGE_SECTOR )	( OTA_IMAGE_BASE + IMAGE_SECTOR_OFFSET( IMAGE_SECTOR ) )
#define BACKUP_IMAGE_SECTOR_ADDRESS( IMAGE_SECTOR )	( BACKUP_IMAGE_BASE + IMAGE_SECTOR_OFFSET( IMAGE_SECTOR ) )

/**
 * @brief Update Flag definitions.
 */
#define SWAP_NOT_STARTED                    ( 0x00U )
#define SWAP_STARTED                        ( 0x80U )
#define IMAGE_SECTOR_5_ERASED               ( 0xC0U )
#define IMAGE_SECTOR_4_ERASED               ( 0xE0U )
#define IMAGE_SECTOR_3_ERASED               ( 0xF0U )
#define IMAGE_SECTOR_2_ERASED               ( 0xF8U )
#define IMAGE_SECTOR_1_ERASED               ( 0xFCU )
#define IMAGE_SECTOR_0_ERASED               ( 0xFEU )
#define SWAP_COMPLETED                      ( 0xFFU )

#define SWAP_FLAGS_OFFSET                   ( 0xA0U )
#define SWAP_FLAG_SIZE                      ( 0x20U )

#define SWAP_STARTED_FLAG                   ( 0x00U )
#define SWAP_IMAGE_SECTOR_5_ERASED_FLAG     ( 0x01U )
#define SWAP_IMAGE_SECTOR_4_ERASED_FLAG     ( 0x02U )
#define SWAP_IMAGE_SECTOR_3_ERASED_FLAG     ( 0x03U )
#define SWAP_IMAGE_SECTOR_2_ERASED_FLAG     ( 0x04U )
#define SWAP_IMAGE_SECTOR_1_ERASED_FLAG     ( 0x05U )
#define SWAP_IMAGE_SECTOR_0_ERASED_FLAG     ( 0x06U )
#define SWAP_COMPLETED_FLAG                 ( 0x07U )

// 0xa0 = 160
#define SWAP_STARTED_FLAG_ADDRESS                   ( SWAP_FLAGS_OFFSET + SWAP_STARTED_FLAG * SWAP_FLAG_SIZE )
// +32
#define SWAP_IMAGE_SECTOR_5_ERASED_FLAG_ADDRESS     ( SWAP_FLAGS_OFFSET + SWAP_IMAGE_SECTOR_5_ERASED_FLAG * SWAP_FLAG_SIZE )
// +64
#define SWAP_IMAGE_SECTOR_4_ERASED_FLAG_ADDRESS     ( SWAP_FLAGS_OFFSET + SWAP_IMAGE_SECTOR_4_ERASED_FLAG * SWAP_FLAG_SIZE )
// + 96
#define SWAP_IMAGE_SECTOR_3_ERASED_FLAG_ADDRESS     ( SWAP_FLAGS_OFFSET + SWAP_IMAGE_SECTOR_3_ERASED_FLAG * SWAP_FLAG_SIZE )
// +128
#define SWAP_IMAGE_SECTOR_2_ERASED_FLAG_ADDRESS     ( SWAP_FLAGS_OFFSET + SWAP_IMAGE_SECTOR_2_ERASED_FLAG * SWAP_FLAG_SIZE )
// + 160
#define SWAP_IMAGE_SECTOR_1_ERASED_FLAG_ADDRESS     ( SWAP_FLAGS_OFFSET + SWAP_IMAGE_SECTOR_1_ERASED_FLAG * SWAP_FLAG_SIZE )
// + 192
#define SWAP_IMAGE_SECTOR_0_ERASED_FLAG_ADDRESS     ( SWAP_FLAGS_OFFSET + SWAP_IMAGE_SECTOR_0_ERASED_FLAG * SWAP_FLAG_SIZE )
// + 224
#define SWAP_COMPLETED_FLAG_ADDRESS                 ( SWAP_FLAGS_OFFSET + SWAP_COMPLETED_FLAG * SWAP_FLAG_SIZE )

#endif /* ifndef _AWS_BOOT_FLASH_INFO_H_ */

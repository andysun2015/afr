/**
  ******************************************************************************
  * File Name          : aws_boot_partition.h
  * Description        : This file provides code that defines FLASH partitions
  *                      on the STM32H743ZI.
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2018 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/**
 * @file aws_boot_partition.h
 * @brief Partition definitions header.
 */

#ifndef _AWS_BOOT_PARTITION_H_
#define _AWS_BOOT_PARTITION_H_

/*ST includes.*/
#include "stm32f7xx_hal.h"

/* Bootloader includes.*/
#include "aws_boot_loader.h"
#include "aws_boot_flash_info.h"

/**
 * @brief Magic code for partition entry.
 */
#define FLASH_PARTITION_MAGIC_CODE            0xCA5CADED


/**
 * @brief Maximum size of partition label.
 */
#define FLASH_PARTITION_LABEL_SIZE            ( 16 )

/**
 * @brief Types and subtypes of partitions.
 */
#define FLASH_PARTITION_TYPE_APP              ( 0 )
#define FLASH_PARTITION_SUBTYPE_APP_OTA       ( 1 )
#define FLASH_PARTITION_SUBTYPE_APP_DEFAULT   ( 2 )

#define FLASH_PARTITION_TYPE_DATA             ( 1 )
#define FLASH_PARTITION_SUBTYPE_DATA_WIFI     ( 1 )
#define FLASH_PARTITION_SUBTYPE_DATA_USER     ( 2 )

/**
 * @brief Structure definition for flash area.
 */
typedef struct
{
    uint8_t ucFlashAreaId;   /**< Flash area ID.*/
    uint8_t ucFlashDeviceId; /**< Flash device ID.*/
    uint32_t ulOffset;       /**< Offset of flash area from base.*/
    uint32_t ulSize;         /**< Size of the flash area.*/
} FLASHArea_t;

/**
 * @brief Structure definition for partition.
 */
typedef struct
{
    uint32_t ulMagic;                                       /**< Magic code for partition entry.*/
    uint8_t ucPartitionLabel[ FLASH_PARTITION_LABEL_SIZE ]; /**< Partition label.*/
    uint8_t ucPartitionType;                                /**< Type of partition.*/
    uint8_t ucPartitionSubType;                             /**< subtype of partition.*/
    FLASHArea_t xFlashArea;                                 /**< Flash area info for partition.*/
    uint32_t ulFlags;                                       /**< partition flags.*/
} FLASHPartition_t;

/**
 * @brief Structure definition for partition info on platform.
 */
typedef struct
{
    void * pvDefaultAppExecAddress;
    BOOTImageDescriptor_t * paxOTAAppDescriptor[ FLASH_PARTITIONS_OTA_MAX ];
    uint8_t ucNumOfApps;
} BOOTPartition_Info_t;

BaseType_t BOOT_FLASH_ReadPartitionTable( BOOTPartition_Info_t * xPartitionInfo );
uint8_t BOOT_FLASH_GetFlashArea( const BOOTImageDescriptor_t * pxAppDesc );

BaseType_t AWS_CopyOTAImageToMainApplication( void );
BaseType_t AWS_RollBackFlashImage( void );

#endif /* ifndef _AWS_BOOT_PARTITION_H_ */

/**
  ******************************************************************************
  * File Name          : aws_boot_partition.h
  * Description        : This file provides code that defines FLASH partitions
  *                      on the STM32H743ZI.
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2018 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */


/* Bootloader includes.*/
#include "aws_boot_partition.h"
#include "aws_boot_types.h"
#include "aws_boot_flash_info.h"

/* Default partition table for STM32H743ZI.
 * The device support two memory areas.
 */
static FLASHPartition_t axPartitonTable[] =
{
    {
        .ulMagic = FLASH_PARTITION_MAGIC_CODE,
        .ucPartitionLabel = "OTA Image 0",
        .ucPartitionType = FLASH_PARTITION_TYPE_APP,
        .ucPartitionSubType = FLASH_PARTITION_SUBTYPE_APP_OTA,
        .xFlashArea =
        {
            .ucFlashAreaId   = FLASH_PARTITION_IMAGE_0,
            .ucFlashDeviceId = FLASH_DEVICE_INTERNAL,
            .ulOffset        = FLASH_PARTITION_OFFSET_IMAGE_0,
            .ulSize          = FLASH_IMAGE_SIZE_MAX,
        },
        .ulFlags = 0xFFFFFFFF,
    },
    {
        .ulMagic = FLASH_PARTITION_MAGIC_CODE,
        .ucPartitionLabel = "OTA Image 1",
        .ucPartitionType = FLASH_PARTITION_TYPE_APP,
        .ucPartitionSubType = FLASH_PARTITION_SUBTYPE_APP_OTA,
        .xFlashArea =
        {
            .ucFlashAreaId   = FLASH_PARTITION_IMAGE_1,
            .ucFlashDeviceId = FLASH_DEVICE_INTERNAL,
            .ulOffset        = FLASH_PARTITION_OFFSET_IMAGE_1,
            .ulSize          = FLASH_IMAGE_SIZE_MAX,
        },
        .ulFlags = 0xFFFFFFFF,
    },
    {
        .ulMagic = FLASH_PARTITION_MAGIC_CODE,
        .ucPartitionLabel = "OTA Image 0'",
        .ucPartitionType = FLASH_PARTITION_TYPE_APP,
        .ucPartitionSubType = FLASH_PARTITION_SUBTYPE_APP_OTA,
        .xFlashArea =
        {
            .ucFlashAreaId   = FLASH_PARTITION_IMAGE_BACKUP,
            .ucFlashDeviceId = FLASH_DEVICE_INTERNAL,
            .ulOffset        = FLASH_PARTITION_OFFSET_IMAGE_BACKUP,
            .ulSize          = FLASH_IMAGE_SIZE_MAX,
        },
        .ulFlags = 0xFFFFFFFF,
    }
};

/*-----------------------------------------------------------*/

uint8_t BOOT_FLASH_GetFlashArea( const BOOTImageDescriptor_t * pxAppDesc )
{
    uint8_t ucIndex;
    uint8_t ucFlashArea = 0xFF;

    void * pvAppDesc = ( void * ) pxAppDesc;

    for( ucIndex = 0; ucIndex < ( sizeof( axPartitonTable ) / sizeof( FLASHPartition_t ) ); ucIndex++ )
    {
        if( axPartitonTable[ ucIndex ].ucPartitionType == FLASH_PARTITION_TYPE_APP )
        {
            if( pvAppDesc == ( void * ) ( FLASH_DEVICE_BASE + axPartitonTable[ ucIndex ].xFlashArea.ulOffset ) )
            {
                ucFlashArea = axPartitonTable[ ucIndex ].xFlashArea.ucFlashAreaId;
                break;
            }
        }
    }

    return ucFlashArea;
}

/*-----------------------------------------------------------*/

BaseType_t BOOT_FLASH_ReadPartitionTable( BOOTPartition_Info_t * xPartitionInfo )
{
    uint8_t i;

    xPartitionInfo->ucNumOfApps = 0;
    
    for( i = 0; i < ( sizeof( axPartitonTable ) / sizeof( FLASHPartition_t ) ); i++ )
    {
        switch( axPartitonTable[ i ].ucPartitionType )
        {
            case FLASH_PARTITION_TYPE_APP:
                xPartitionInfo->ucNumOfApps++;

                if( axPartitonTable[ i ].ucPartitionSubType & FLASH_PARTITION_SUBTYPE_APP_OTA )
                {
                    xPartitionInfo->paxOTAAppDescriptor[ i ] =
                        ( void * ) ( FLASH_DEVICE_BASE + axPartitonTable[ i ].xFlashArea.ulOffset );
                }

                if( axPartitonTable[ i ].ucPartitionSubType & FLASH_PARTITION_SUBTYPE_APP_DEFAULT )
                {
                    xPartitionInfo->pvDefaultAppExecAddress =
                        ( void * ) ( FLASH_DEVICE_BASE + axPartitonTable[ i ].xFlashArea.ulOffset + sizeof( BOOTImageDescriptor_t ) );
                }

                break;

            case FLASH_PARTITION_TYPE_DATA:
                /* Not used.*/
                break;

            default:

                /* Incorrect partition type.*/
                return pdFALSE;
        }
    }

    xPartitionInfo->ucNumOfApps -= 1;

    return pdTRUE;
}

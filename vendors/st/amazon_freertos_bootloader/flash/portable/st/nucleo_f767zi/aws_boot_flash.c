/**
  ******************************************************************************
  * File Name          : aws_boot_flash.c
  * Description        : This file provides code that interacts with the FLASH
  *                      memory on the STM32H743ZI.
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2018 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */


/**
 * @file aws_boot_flash.c
 * @brief Boot flash implementation.
 */

/* ST includes.*/
#include "stm32f7xx_hal.h"

/* Bootloader includes.*/
#include "aws_boot_types.h"
#include "aws_boot_flash.h"
#include "aws_boot_loader.h"
#include "aws_boot_partition.h"
#include "aws_boot_flash_info.h"
#include "aws_boot_log.h"
#include "aws_boot_config.h"

/*-----------------------------------------------------------*/

BaseType_t AWS_FlashErasePartition( uint8_t ucFlashArea );

BaseType_t BOOT_FLASH_Write( const uint32_t * pulAddress,
                             const uint32_t * pulData,
                             int lLength )
{
    BaseType_t xReturn = pdFALSE;
    
    /* Flash write must occur on a write boundary (256 bit) and a full 256 bits
       must be written. */
    //if ( ( ( (uint32_t) pulAddress % FLASHWORD_SIZE) == 0 ) &&
    //     ( ( lLength  % FLASHWORD_SIZE ) == 0 ) )
    //{
        // word-aligned
    	//uint32_t ulBlocks = lLength / FLASHWORD_SIZE;
    	uint32_t ulBlocks = (uint32_t) lLength;
        uint32_t ulAddress = (uint32_t) pulAddress;
        uint32_t ulData = (uint32_t) pulData;
        
        HAL_FLASH_Unlock();
        
        xReturn = pdTRUE;
        
        while ( ulBlocks-- )
        {
            //if ( HAL_OK != HAL_FLASH_Program( FLASH_TYPEPROGRAM_FLASHWORD, ulAddress, ulData ) )
            if ( HAL_OK != HAL_FLASH_Program( FLASH_TYPEPROGRAM_BYTE, ulAddress, ((uint64_t)(*(uint8_t *)ulData)) ) )
            {
                xReturn = pdFALSE;
                break;
            }
            else
            {
            	// word-aligned
                //ulAddress += FLASHWORD_SIZE;
                ulAddress++;
                //  word-aligned
                //ulData += FLASHWORD_SIZE;
                ulData++;
            }
        }
        
        HAL_FLASH_Lock();
    //}
    
    return xReturn;
}
/*-----------------------------------------------------------*/

BaseType_t BOOT_FLASH_EraseHeader( const BOOTImageDescriptor_t * pxAppDescriptor )
{
    DEFINE_BOOT_METHOD_NAME( "BOOT_FLASH_EraseHeader" );

    BaseType_t xReturn = pdTRUE;

    BOOT_LOG_L3( "[%s] This function is kept for compatibility with the generic bootloader.\r\n",
                         BOOT_METHOD_NAME );
    
    return xReturn;
}
/*-----------------------------------------------------------*/

BaseType_t BOOT_FLASH_EraseBank( const BOOTImageDescriptor_t * pxAppDescriptor )
{
    DEFINE_BOOT_METHOD_NAME( "BOOT_FLASH_EraseBank" );
    
    BaseType_t xReturn = pdFALSE;
    uint8_t ucFlashArea;

    ucFlashArea = BOOT_FLASH_GetFlashArea( pxAppDescriptor );

    switch( ucFlashArea )
    {
		case( FLASH_PARTITION_IMAGE_0 ):
		{
#if 0
			BOOT_LOG_L3( "[%s] Restoring backup image to : 0x%08x\r\n", BOOT_METHOD_NAME, pxAppDescriptor );

			xReturn = AWS_RollBackFlashImage();

			if(xReturn == pdTRUE)
			{
				xReturn = AWS_FlashErasePartition( FLASH_PARTITION_IMAGE_1 );
			}
			if(xReturn == pdTRUE)
			{
				xReturn = AWS_FlashErasePartition( FLASH_PARTITION_IMAGE_BACKUP );
			}
#endif

			xReturn = pdTRUE;

			break;
		}
		case( FLASH_PARTITION_IMAGE_1 ):
		{
			xReturn = AWS_FlashErasePartition( FLASH_PARTITION_IMAGE_1 );

			if(xReturn == pdTRUE)
			{
				xReturn = AWS_FlashErasePartition( FLASH_PARTITION_IMAGE_BACKUP );
			}

			if(xReturn == pdTRUE)
			{
				BOOT_LOG_L2( "[%s] Bank erased at : 0x%08x\r\n",BOOT_METHOD_NAME, pxAppDescriptor);
			}
			else
			{
				BOOT_LOG_L2( "[%s] Bank erase failed at : 0x%08x\r\n",BOOT_METHOD_NAME, pxAppDescriptor);
			}
			break;
		}
		default:
		{
			break;
		}
    }

    return xReturn;
}

/*-----------------------------------------------------------*/

BaseType_t BOOT_FLASH_ValidateAddress( const BOOTImageDescriptor_t * pxAppDescriptor )
{
    DEFINE_BOOT_METHOD_NAME( "BOOT_FLASH_ValidateAddress" );

    BaseType_t xReturn = pdFALSE;

    /* Application start address needs to be >= than this limit. */
    const void * const pvStartAddressLimit = ( const void * ) ( FLASH_DEVICE_BASE + FLASH_PARTITION_OFFSET_IMAGE_0 + sizeof( BOOTImageHeader_t ) );

    /* End address needs to be < than this limit.*/
    const void * const pvEndAddressLimit = ( const void * ) ( FLASH_DEVICE_BASE + FLASH_PARTITION_OFFSET_IMAGE_0 + FLASH_IMAGE_SIZE_MAX );

    /* Validate the start address.*/
    xReturn = ( ( pxAppDescriptor->pvStartAddress >= pvStartAddressLimit ) &&
                ( pxAppDescriptor->pvStartAddress < pvEndAddressLimit ) );

    BOOT_LOG_L3( "[%s] Start Address: 0x%08x\r\n",
                 BOOT_METHOD_NAME,
                 pxAppDescriptor->pvStartAddress );

    /* Validate the end address.*/
    xReturn = xReturn && ( ( pxAppDescriptor->pvEndAddress > pvStartAddressLimit ) &&
                           ( pxAppDescriptor->pvEndAddress <= pvEndAddressLimit ) );

    BOOT_LOG_L3( "[%s] End Address: 0x%08x\r\n",
                 BOOT_METHOD_NAME,
                 pxAppDescriptor->pvEndAddress );


    /* Validate the execution address.*/
    xReturn = xReturn && ( ( pxAppDescriptor->pvExecAddress >= pxAppDescriptor->pvStartAddress ) &&
                           ( pxAppDescriptor->pvExecAddress < pxAppDescriptor->pvEndAddress ) );

    BOOT_LOG_L3( "[%s] Exe Address: 0x%08x\r\n",
                 BOOT_METHOD_NAME,
                 pxAppDescriptor->pvExecAddress );
    
     /* Validate the start address is less than end address.*/
    xReturn = xReturn &&  ( pxAppDescriptor->pvStartAddress < pxAppDescriptor->pvEndAddress );

    return xReturn;
}
/*-----------------------------------------------------------*/

BaseType_t AWS_FlashErasePartition( uint8_t ucFlashArea )
{
  uint32_t ulError = 0UL;
  
  /* Partition Image 0 always occupies the same region in FLASH_BANK1. */
  if ( FLASH_PARTITION_IMAGE_0 == ucFlashArea )
  {
    FLASH_EraseInitTypeDef xEraseInit =
    {
      .TypeErase      = FLASH_TYPEERASE_SECTORS,
      .Banks          = FLASH_BANK_1,
      .Sector         = FLASH_SECTOR_6,
      .NbSectors      = 6,
      .VoltageRange   = FLASH_VOLTAGE_RANGE_3
    };
    
    HAL_FLASH_Unlock();
    
    if ( HAL_OK != HAL_FLASHEx_Erase( &xEraseInit, &ulError ) )
    {
      return pdFAIL;
    }
    if (0xFFFFFFFFUL != ulError )
    {
      return pdFAIL;
    }
  }
  /* Since the STM32H743 performs a sector by sector swap of OTA firmware images,
     Partition Image 1 only refers to a new OTA image and not a rollback image.
     As such, only the first sector of the image (with the header) must be
     erased. */
  else if ( FLASH_PARTITION_IMAGE_1 == ucFlashArea )
  {
    FLASH_EraseInitTypeDef xEraseInit =
    {
      .TypeErase      = FLASH_TYPEERASE_SECTORS,
      .Banks          = FLASH_BANK_2,
      .Sector         = FLASH_SECTOR_18,
      .NbSectors      = 1,
      .VoltageRange   = FLASH_VOLTAGE_RANGE_3
    };
    
    HAL_FLASH_Unlock();
    
    if ( HAL_OK != HAL_FLASHEx_Erase( &xEraseInit, &ulError ) )
    {
      return pdFAIL;
    }
    if (0xFFFFFFFFUL != ulError )
    {
      return pdFAIL;
    }
  }
  /* Since the STM32H743 performs a sector by sector swap of OTA firmware images,
     Partition Image 2 only refers to a rollback image and not a new OTA image.
     This partition should only be erased if there is no valid Partition Image 1. */
  else if ( FLASH_PARTITION_IMAGE_BACKUP == ucFlashArea )
  {
    BOOTImageDescriptor_t * pxPartitionImage1 = ( void * ) ( FLASH_DEVICE_BASE + FLASH_PARTITION_OFFSET_IMAGE_1 );
    
    if( memcmp( pxPartitionImage1->xImageHeader.acMagicCode,
                BOOT_MAGIC_CODE,
                BOOT_MAGIC_CODE_SIZE ) != 0 )
    {
        FLASH_EraseInitTypeDef xEraseInit =
        {
            .TypeErase      = FLASH_TYPEERASE_SECTORS,
            .Banks          = FLASH_BANK_2,
            .Sector         = FLASH_SECTOR_19,
            .NbSectors      = 5,
            .VoltageRange   = FLASH_VOLTAGE_RANGE_3
        };
        
        HAL_FLASH_Unlock();
        
        if ( HAL_OK != HAL_FLASHEx_Erase( &xEraseInit, &ulError ) )
        {
          return pdFAIL;
        }
        if (0xFFFFFFFFUL != ulError )
        {
          return pdFAIL;
        }
    }
  }
  else
  {
    return pdFAIL;
  }
  
  HAL_FLASH_Lock();
  
  return pdPASS;
}

/*-----------------------------------------------------------*/

uint32_t SwapStep[12][4] =
{
	// f767 use actual sector 17 as swap
	// define bank 1/2 as 1st digit, define image sector 1 - 6 respectively, define swap sector as 0(bank 2 only - 20)
    // step 0 : 16 to 20 (swap sector)
	// 20 = actual sector 17
    { FLASH_BANK_2, FLASH_SECTOR_17, 0x08120000UL, 0x080E0000UL },
	// step 1 : 26 to 16, 26 erased
	// 16 = actual sector 11
	{ FLASH_BANK_1, FLASH_SECTOR_11, 0x080E0000UL, 0x081E0000UL },
    // step 2 : 15 to 26
	// 26 = actual sector 23
	{ FLASH_BANK_2, FLASH_SECTOR_23, 0x081E0000UL, 0x080C0000UL },
	// step 3 : 25 to 15, 25 erased
	// 15 = actual sector 10
	{ FLASH_BANK_1, FLASH_SECTOR_10, 0x080C0000UL, 0x081C0000UL },
	// step 4 : 14 to 25
	// 25 = actual sector 22
	{ FLASH_BANK_2, FLASH_SECTOR_22, 0x081C0000UL, 0x080A0000UL },
	// step 5 : 24 to 14, 24 erased
	// 14 = actual sector 9
	{ FLASH_BANK_1, FLASH_SECTOR_9, 0x080A0000UL, 0x081A0000UL },
	// step 6 : 13 to 24
	// 24 = actual sector 21
	{ FLASH_BANK_2, FLASH_SECTOR_21, 0x081A0000UL, 0x08080000UL },
	// step 7 : 23 to 13, 23 erased
	// 13 = actual sector 8
	{ FLASH_BANK_1, FLASH_SECTOR_8, 0x08080000UL, 0x08180000UL },
	// step 8 : 12 to 23
	// 23 = actual sector 20
	{ FLASH_BANK_2, FLASH_SECTOR_20, 0x08180000UL, 0x08060000UL },
	// step 9 : 22 to 12, 22 erased
	// 12 = actual sector 7
	{ FLASH_BANK_1, FLASH_SECTOR_7, 0x08060000UL, 0x08160000UL },
	// step 10 : 11 to 22
	// 22 = actual sector 19
	{ FLASH_BANK_2, FLASH_SECTOR_19, 0x08160000UL, 0x08040000UL },
	// step 11 : 21 to 11, 21 erased
	// 11 = actual sector 6
	{ FLASH_BANK_1, FLASH_SECTOR_6, 0x08040000UL, 0x08140000UL }
};

BaseType_t AWS_FlashEraseSector( uint32_t ulBank, uint32_t ulSector )
{
    BaseType_t xReturn = pdPASS;
    
    uint32_t ulError = 0UL;
    
    FLASH_EraseInitTypeDef xEraseInit =
    {
        .TypeErase    = FLASH_TYPEERASE_SECTORS,
        .Banks        = ulBank,
        .Sector       = ulSector,
        .NbSectors    = 1,
        .VoltageRange = FLASH_VOLTAGE_RANGE_3
    };
    
    HAL_FLASH_Unlock();
    
    if ( HAL_OK != HAL_FLASHEx_Erase( &xEraseInit, &ulError ) )
    {
        xReturn = pdFAIL;
    }
    
    if (0xFFFFFFFFUL != ulError )
    {
        xReturn = pdFAIL;
    }
    
    HAL_FLASH_Lock();
    
    return xReturn;
}

BaseType_t AWS_FlashSwapSetFlag( BaseType_t Flag )
{
	BaseType_t xReturn = pdFAIL;

	uint32_t * Address = NULL;
	// uint32_t Zeroes = 0UL;
	// for f767, write 32 byte 0x00 as flag
	uint8_t Zeroes[32] = { 0 };

	switch( Flag )
	{
		case( SWAP_STARTED ):
		{
			Address = (uint32_t *) ( OTA_IMAGE_BASE + SWAP_STARTED_FLAG_ADDRESS );
			break;
		}
		case( IMAGE_SECTOR_5_ERASED ):
		{
			Address = (uint32_t *) ( OTA_IMAGE_BASE + SWAP_IMAGE_SECTOR_5_ERASED_FLAG_ADDRESS );
			break;
		}
		case( IMAGE_SECTOR_4_ERASED ):
		{
			Address = (uint32_t *) ( OTA_IMAGE_BASE + SWAP_IMAGE_SECTOR_4_ERASED_FLAG_ADDRESS );
			break;
		}
		case( IMAGE_SECTOR_3_ERASED ):
		{
			Address = (uint32_t *) ( OTA_IMAGE_BASE + SWAP_IMAGE_SECTOR_3_ERASED_FLAG_ADDRESS );
			break;
		}
		case( IMAGE_SECTOR_2_ERASED ):
		{
			Address = (uint32_t *) ( OTA_IMAGE_BASE + SWAP_IMAGE_SECTOR_2_ERASED_FLAG_ADDRESS );
			break;
		}
		case( IMAGE_SECTOR_1_ERASED ):
		{
			Address = (uint32_t *) ( OTA_IMAGE_BASE + SWAP_IMAGE_SECTOR_1_ERASED_FLAG_ADDRESS );
			break;
		}
		case( IMAGE_SECTOR_0_ERASED ):
		{
			Address = (uint32_t *) ( OTA_IMAGE_BASE + SWAP_IMAGE_SECTOR_0_ERASED_FLAG_ADDRESS );
			break;
		}
		case( SWAP_COMPLETED ):
		{
			Address = (uint32_t *) ( MAIN_IMAGE_BASE + SWAP_COMPLETED_FLAG_ADDRESS );
			break;
		}
		default:
		{
			return pdFAIL;
		}
	}

	xReturn = BOOT_FLASH_Write( Address, (uint32_t *)&Zeroes, 32 );

	return xReturn;
}

BaseType_t AWS_FlashSwapSectors( uint32_t Step )
{
    BaseType_t xReturn = pdFAIL;
    
    xReturn = AWS_FlashEraseSector( SwapStep[Step][0], SwapStep[Step][1] );
    
    if ( pdPASS == xReturn )
    {
        uint32_t * pulAddress = (uint32_t *) SwapStep[Step][2];
        uint32_t * pulData    = (uint32_t *) SwapStep[Step][3];
        int32_t lLength       = FLASH_SECTOR_SIZE;
        
        xReturn = BOOT_FLASH_Write( pulAddress,
                                    pulData,
                                    lLength );
    }
    
    return pdPASS;
}

BaseType_t AWS_CopyOTAImageToMainApplication( void )
{
    BaseType_t xReturn = pdPASS;
    uint8_t ucStep = 0U;
    
    do
    {
        if (0 == ucStep)
        {
        	AWS_FlashSwapSetFlag( SWAP_STARTED );
        }
        if (1 == ucStep)
        {
        	AWS_FlashSwapSetFlag( IMAGE_SECTOR_5_ERASED );
        }
        if (3 == ucStep)
        {
        	AWS_FlashSwapSetFlag( IMAGE_SECTOR_4_ERASED );
        }
        if (5 == ucStep)
        {
        	AWS_FlashSwapSetFlag( IMAGE_SECTOR_3_ERASED );
        }
        if (7 == ucStep)
        {
        	AWS_FlashSwapSetFlag( IMAGE_SECTOR_2_ERASED );
        }
        if (9 == ucStep)
        {
        	AWS_FlashSwapSetFlag( IMAGE_SECTOR_1_ERASED );
        }
        if (11 == ucStep)
        {
        	AWS_FlashSwapSetFlag( IMAGE_SECTOR_0_ERASED );
        }
        
        xReturn = AWS_FlashSwapSectors( ucStep );
    } while ( ( ++ucStep < 12 ) && ( pdPASS == xReturn ) );
    
    /* Erase the first OTA image sector when complete. */
//    if ( ( pdPASS == xReturn ) &&
//             ( pdFAIL == AWS_FlashEraseSector( FLASH_BANK_2, OTA_IMAGE_SECTOR_ADDRESS( IMAGE_SECTOR_0 ) ) ) )
    // for f767 dual bank, 1st ota image sector is on actual sector 18
    if ( ( pdPASS == xReturn ) && 
         ( pdFAIL == AWS_FlashEraseSector( FLASH_BANK_2, FLASH_SECTOR_18 ) ) )
    {
        xReturn = pdFAIL;
    }
    else
    {
    	AWS_FlashSwapSetFlag( SWAP_COMPLETED );
    }
    
    return xReturn;
}

//#define SWAP_NOT_STARTED            (0x00U)
//#define SWAP_STARTED                (0x80U)
#define SECTOR_7_ERASED             (0xC0U)
#define SECTOR_6_ERASED             (0xE0U)
#define SECTOR_5_ERASED             (0xF0U)
#define SECTOR_4_ERASED             (0xF8U)
#define SECTOR_3_ERASED             (0xFCU)
#define SECTOR_2_ERASED             (0xFEU)
//#define SWAP_COMPLETED              (0xFFU)

uint8_t prvGetCurrentSwapStage( void )
{
    uint8_t ucStage = SWAP_COMPLETED;
    
    //if ( * (uint32_t *) 0x08140180UL == 0x00000000UL )
    {
    //    ucStage = SWAP_COMPLETED;
    }
    //else if ( * (uint32_t *) 0x08140160UL == 0x00000000UL )
    if ( * (uint32_t *) 0x08140160UL == 0x00000000UL )
    {
        ucStage = SECTOR_2_ERASED;
    }
    else if ( * (uint32_t *) 0x08140140UL == 0x00000000UL )
    {
        ucStage = SECTOR_3_ERASED;
    }
    else if ( * (uint32_t *) 0x08140120UL == 0x00000000UL )
    {
        ucStage = SECTOR_4_ERASED;
    }
    else if ( * (uint32_t *) 0x08140100UL == 0x00000000UL )
    {
        ucStage = SECTOR_5_ERASED;
    }
    else if ( * (uint32_t *) 0x081400E0UL == 0x00000000UL )
    {
        ucStage = SECTOR_6_ERASED;
    }
    else if ( * (uint32_t *) 0x081400C0UL == 0x00000000UL )
    {
        ucStage = SECTOR_7_ERASED;
    }
    else if ( * (uint32_t *) 0x081400A0UL == 0x00000000UL )
    {
        ucStage = SWAP_STARTED;
    }
    
    return ucStage;
}

BaseType_t prvRollBackSector( uint8_t ucSector )
{
    BaseType_t xReturn = pdFAIL;
    
    // better swap back to h7 partition which is more concise and elegant
    // actual sector number = h7 sector number + 4 for bank 1 on f7
    xReturn = AWS_FlashEraseSector( FLASH_BANK_1, ucSector + 4 );
    
    if (( pdPASS == xReturn ) && ( ucSector != 7 ))
    {
      uint32_t * pulAddress = (uint32_t *) ( FLASH_DEVICE_BASE + ucSector * FLASH_SECTOR_SIZE );
      uint32_t * pulData    = (uint32_t *) ( FLASH_DEVICE_BASE + FLASH_BANK_SIZE + (ucSector + 1 ) * FLASH_SECTOR_SIZE );
      int32_t const lLength = FLASH_SECTOR_SIZE;

      xReturn = BOOT_FLASH_Write( pulAddress,
                                 pulData,
                                 lLength );
    }
    else if (( pdPASS == xReturn ) && ( ucSector == 7 ))
    {
      uint32_t * pulAddress = (uint32_t *) ( FLASH_DEVICE_BASE + ucSector * FLASH_SECTOR_SIZE );
      uint32_t * pulData    = (uint32_t *) ( FLASH_DEVICE_BASE + FLASH_BANK_SIZE + (ucSector - 6 ) * FLASH_SECTOR_SIZE );
      int32_t const lLength = FLASH_SECTOR_SIZE;
      
      xReturn = BOOT_FLASH_Write( pulAddress,
                                 pulData,
                                 lLength );
    }
    
    return xReturn;
}

BaseType_t AWS_RollBackFlashImage( void )
{
    BaseType_t xReturn = pdPASS;
    
    uint8_t ucStep = prvGetCurrentSwapStage();
    
    if ( ( pdFAIL != xReturn ) && ( SECTOR_2_ERASED == (SECTOR_2_ERASED & ucStep ) ) )
    {
    	// h7 sector 2
        xReturn = prvRollBackSector( FLASH_SECTOR_2 );
    }
    if ( ( pdFAIL != xReturn ) && ( SECTOR_3_ERASED == (SECTOR_3_ERASED & ucStep ) ) )
    {
    	// h7 sector 3
        xReturn = prvRollBackSector( FLASH_SECTOR_3 );
    }
    if ( ( pdFAIL != xReturn ) && ( SECTOR_4_ERASED == (SECTOR_4_ERASED & ucStep ) ) )
    {
    	// h7 sector 4
        xReturn = prvRollBackSector( FLASH_SECTOR_4 );
    }
    if ( ( pdFAIL != xReturn ) && (  SECTOR_5_ERASED == (SECTOR_5_ERASED & ucStep ) ) )
    {
    	// h7 sector 5
        xReturn = prvRollBackSector( FLASH_SECTOR_5 );
    }
    if ( ( pdFAIL != xReturn ) && ( SECTOR_6_ERASED == (SECTOR_6_ERASED & ucStep ) ) )
    {
    	// h7 sector 6
        xReturn = prvRollBackSector( FLASH_SECTOR_6 );
    }
    if ( ( pdFAIL != xReturn ) && ( SECTOR_7_ERASED == (SECTOR_7_ERASED & ucStep ) ) )
    {
    	// h7 sector 7
        xReturn = prvRollBackSector( FLASH_SECTOR_7 );
    }
    if ( ( pdFAIL != xReturn ) && ( SWAP_STARTED == (SWAP_STARTED & ucStep ) ) )
    {
        xReturn = AWS_FlashErasePartition( FLASH_PARTITION_IMAGE_1 );
        
        if ( pdPASS == xReturn )
        {
            xReturn = AWS_FlashErasePartition( FLASH_PARTITION_IMAGE_BACKUP );
        }
    }
    
    return xReturn;
}
